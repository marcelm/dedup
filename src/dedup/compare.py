#!/usr/bin/env python3
"""
Compare two directory trees
"""
import os
from fnmatch import fnmatch
import logging

from .utils import create_index

logger = logging.getLogger(__name__)


def add_arguments(parser):
    arg = parser.add_argument
    arg(
        "--ignore",
        dest="ignore_pattern",
        default=None,
        help="A pattern that describes files to ignore. For ex., '*.svn-base'",
    )
    arg(
        "--minsize",
        type=int,
        default=256,
        help="Minimum file size. Smaller files are ignored (default: %(default)s).",
    )
    arg("first", help="First directory")
    arg("second", help="Second directory")


def main(args):
    for path in [args.first, args.second]:
        if not os.path.isdir(path):
            raise ValueError(f"{path} is not a directory")

    logger.info("Indexing first directory ...")
    fileset1 = create_index(args.first, args.minsize, args.ignore_pattern)

    logger.info("Indexing second directory ...")
    fileset2 = create_index(args.second, args.minsize, args.ignore_pattern)

    logger.info("Comparing ...")
    both1 = 0
    only1 = 0
    for path in fileset1:
        if fileset2.duplicates_of(path):
            both1 += 1
        else:
            only1 += 1

    both2 = 0
    only2 = 0
    for path in fileset2:
        if fileset1.duplicates_of(path):
            both2 += 1
        else:
            only2 += 1
    print()
    print("First directory {!r}:".format(args.first))
    print("{:7} files in {:.2f} MB".format(len(fileset1), fileset1.size() / 1e6))
    print("{:7} files exist only here".format(only1))
    print("{:7} files shared with other directory".format(both1))
    print("{:7} internal duplicates".format(fileset1.duplicates()))
    print()
    print("Second directory {!r}".format(args.second))
    print("{:7} files in {:.2f} MB".format(len(fileset2), fileset2.size() / 1e6))
    print("{:7} files exist only here".format(only2))
    print("{:7} files shared with other directory".format(both2))
    print("{:7} internal duplicates".format(fileset2.duplicates()))
