#!/usr/bin/env python3
"""
Find files that have identical file modification dates
"""
import os
import logging
from collections import defaultdict
import time

from .utils import walk_files

logger = logging.getLogger(__name__)


def add_arguments(parser):
    arg = parser.add_argument
    arg(
        "--ignore",
        dest="ignore_pattern",
        default=None,
        help="A pattern that describes files to ignore. For ex., '*.svn-base'",
    )
    arg(
        "--minsize",
        type=int,
        default=256,
        help="Minimum file size. Smaller files are ignored (default: %(default)s).",
    )
    arg("first", help="First directory")
    arg("second", help="Second directory")


def main(args):
    for path in [args.first, args.second]:
        if not os.path.isdir(path):
            raise CommandLineError(f"{path} is not a directory")

    logger.info("Indexing first directory ...")
    index = make_date_index(args.first, args.ignore_pattern, args.minsize)
    logger.info("Inspecting second directory ...")
    for path in walk_files(args.second, args.ignore_pattern, args.minsize):
        partners = index.partners_of(path)
        if partners:
            print(path, partners)


def make_date_index(dirpath, ignore_pattern, minsize):
    dateindex = FileDateIndex()
    now = time.time()
    for path in walk_files(dirpath, ignore_pattern, minsize):
        if os.stat(path).st_mtime < now:
            dateindex.add(path)
    return dateindex


class FileDateIndex:
    def __init__(self):
        # map file modification time to list of paths
        self._mtimes = defaultdict(list)

    def add(self, path):
        mtime = os.stat(path).st_mtime
        self._mtimes[mtime].append(path)

    def partners_of(self, path):
        mtime = os.stat(path).st_mtime
        return self._mtimes.get(mtime, [])
