from collections import defaultdict
import os
import hashlib
import logging
from fnmatch import fnmatch

logger = logging.getLogger(__name__)


def filehash(path, n=None):
    """
    Return a checksum (as human-readable hex string) of the first
    n bytes of the file named by 'path'.
    If n is None, the checksum of the entire file is returned.
    """
    # read file in 1 MiB chunks
    chunksize = 1024 * 1024
    with open(path, "rb") as f:
        h = hashlib.md5()
        if n is None:
            while True:
                chunk = f.read(chunksize)
                h.update(chunk)
                if len(chunk) < chunksize:
                    break
        else:
            h.append(f.read(n))
    return h.hexdigest()


class FileInfo:
    __slots__ = ("path", "_stat", "mtime", "size")

    _hash_cache = dict()
    _head_cache = dict()

    def __init__(self, path):
        self.path = path
        self._stat = os.stat(path)
        self.mtime = self._stat.st_mtime
        self.size = self._stat.st_size

    def checksum(self):
        """
        Return checksum of the file.

        The value is cached.
        """
        if self.path not in FileInfo._hash_cache:
            FileInfo._hash_cache[self.path] = filehash(self.path)
        return FileInfo._hash_cache[self.path]

    def head(self):
        """
        Return first 128 bytes of file content

        The value is cached.
        """
        if self.path not in FileInfo._head_cache:
            with open(self.path, "rb") as f:
                FileInfo._head_cache[self.path] = f.read(128)
        return FileInfo._head_cache[self.path]

    def __eq__(self, other):
        if os.path.samestat(self._stat, other._stat):
            return True
        if self.head() != other.head():
            return False
        return self.checksum() == other.checksum()


class FileSet:
    """
    Keep track of a set of files.
    """

    def __init__(self):
        # maps file sizes to lists of paths
        # self.sizes.values() contains all paths that have been added to this set
        self.sizes = defaultdict(list)
        self._infos = dict()
        self._total_size = 0
        self._unique_paths = set()

    def size(self):
        """Return sum of file sizes, in bytes"""
        return self._total_size

    def duplicates(self):
        """
        Return number of 'internal' duplicates.

        A path in this set is an internal duplicate if a second path exists
        that points to a file with the same content as the first path.
        """
        return len(self._infos) - len(self._unique_paths)

    def __iter__(self):
        for paths in self.sizes.values():
            yield from paths

    def checksum(self, path):
        """
        Return (possibly cached) checksum of the file.
        """
        if path in self._infos:
            return self._infos[path].checksum()
        else:
            return FileInfo(path).checksum()

    def add(self, path):
        """
        Add a single path to this set.
        """
        info = FileInfo(path)
        is_unique = True
        for existing in self.sizes.get(info.size, []):
            if self._infos[existing] == info:
                self._unique_paths.discard(existing)
                is_unique = False
        if is_unique:
            self._unique_paths.add(path)
        self.sizes[info.size].append(path)  # defaultdict
        self._infos[path] = info
        self._total_size += info.size

    def duplicates_of(self, path):
        """
        Return a list of all duplicates of the file named by the path.
        Duplicates are those files that have already been added to this
        FileSet instance and that have the same checksum as the given file.

        If path itself has previously been added to this set, it is included
        in the returned list.
        """
        info = FileInfo(path)
        candidates = self.sizes.get(info.size, [])
        return [c for c in candidates if self._infos[c] == info]

    def __len__(self):
        return len(self._infos)


def walk_files(path, ignore_pattern=None, minsize=0):
    for root, dirs, files in os.walk(path):
        for f in files:
            if ignore_pattern and fnmatch(f, ignore_pattern):
                continue
            path = os.path.join(root, f)
            if os.path.islink(path):
                # skip symlinks
                continue
            if os.stat(path).st_size < minsize:
                continue
            yield path


def create_index(path, minsize=100, ignore_pattern=None):
    fileset = FileSet()
    for path in walk_files(path, ignore_pattern, minsize):
        fileset.add(path)
    return fileset


def format_path(path):
    """Format path for printing TODO"""
    return path.encode("utf-8", errors="replace").decode("utf-8")
