#!/usr/bin/env python3
"""
Find youngest files in a directory
"""
import os
from itertools import chain
import time
import logging

from .utils import format_path

logger = logging.getLogger(__name__)


def add_arguments(parser):
    arg = parser.add_argument
    parser.add_argument(
        "-n", type=int, default=20, help="Number of youngest files to show"
    )
    parser.add_argument("dirnames", nargs="+", help="Directory names")


def main(args):
    for path in args.dirnames:
        if not os.path.isdir(path):
            parser.error("{} is not a directory".format(path))

    now = time.time()
    youngest = []
    for root, dirs, files in chain(*(os.walk(path) for path in args.dirnames)):
        for f in files:
            # if ignore_pattern and fnmatch(f, ignore_pattern):
            # continue
            path = os.path.join(root, f)
            if os.path.islink(path):
                # skip symlinks
                continue
            mtime = os.stat(path).st_mtime
            if len(youngest) < args.n:
                youngest = sorted(youngest + [(mtime, path)])
            else:
                if mtime > youngest[0][0] and mtime < now:
                    youngest = sorted(youngest[1:] + [(mtime, path)])
    for mtime, path in youngest:
        ptime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(mtime))
        print(ptime, format_path(path))
