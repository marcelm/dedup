#!/usr/bin/env python3
"""
Create a file database.
"""
__author__ = "Marcel Martin"
__version__ = "0.1"

from argparse import ArgumentParser
import sys
import os
import subprocess

# from os.path import join, getsize
import hashlib

# from collections import defaultdict
# import time
# import random
import sqlite3

from .utils import filehash


class Error(Exception):
    pass


def trash(path, pretend=True):
    command = ["trash", "--", path]
    print(*command)
    if not pretend:
        subprocess.call(command)


class FileDB:
    def __init__(self):
        self.connection = sqlite3.connect(os.path.expanduser("~/.cache/filedb.sdb"))
        self.cursor = self.connection.cursor()
        self.cursor.execute(
            "CREATE TABLE IF NOT EXISTS files (path TEXT PRIMARY KEY, size INTEGER, mtime INTEGER, inode INTEGER, hash)"
        )
        self.cursor.execute(
            "CREATE TABLE IF NOT EXISTS indexed_dirs (path TEXT UNIQUE)"
        )
        self.cursor.execute("CREATE INDEX IF NOT EXISTS hash_idx ON files (hash)")
        self._n = 0

    def add(self, path):
        """Add file."""
        path = os.path.realpath(path)
        stat = os.stat(path)
        size = os.path.getsize(path)
        mtime = int(stat.st_mtime)

        try:
            self.cursor.execute("SELECT mtime FROM files WHERE path = ?", [path])
        except UnicodeEncodeError as e:
            print(path.encode("ascii", "ignore"))
            raise
        rows = list(self.cursor)
        assert len(rows) <= 1
        if len(rows) == 1 and rows[0][0] == mtime:
            # mtime hasn't changed, no need to add this path
            return False
        hash = filehash(path)
        inode = stat.st_ino
        self.cursor.execute(
            "INSERT OR REPLACE INTO files (path, size, mtime, inode, hash) VALUES (?, ?, ?, ?, ?)",
            (path, size, mtime, inode, hash),
        )
        self._n += 1
        if self._n == 10000:
            self.commit()
            self._n = 0
        return True

    def add_directory(self, dirname):
        """
        Recursively add all files within the given directory to the index.
        """
        dirname = os.path.realpath(dirname)
        self.cursor.execute(
            "INSERT OR REPLACE INTO indexed_dirs (path) VALUES (?)", (dirname,)
        )
        errors = 0
        n = 0
        for root, dirs, files in os.walk(dirname):
            for f in files:
                path = os.path.join(root, f)
                # skip fifos etc
                if not os.path.isfile(path):
                    continue
                try:
                    if self.add(path):
                        print(path)
                        n += 1
                except OSError:
                    print("could not index", path)
                    errors += 1
                if n % 1000 == 0:
                    self.commit()
        self.commit()
        return n, errors

    def commit(self):
        self.connection.commit()

    def search_hash(self, hash):
        self.cursor.execute("SELECT path FROM files WHERE hash = ?", (hash,))
        for row in self.cursor:
            yield row[0]

    def all(self):
        "Yield (path, hash) tuples of all entries in the index"
        self.cursor.execute("SELECT path, hash FROM files")
        for row in self.cursor:
            yield row

    def count(self):
        "Count paths in the index"
        return next(self.cursor.execute("SELECT count(*) FROM files"))[0]

    def dirs(self):
        """Return list of indexed directories"""
        self.cursor.execute("SELECT path FROM indexed_dirs")
        dirs = [row[0] for row in self.cursor]
        return dirs

    def close(self):
        self.connection.close()


def cmd_index(args):
    for dirname in args.dirnames:
        if not os.path.isdir(dirname):
            parser.error("{} is not a directory".format(dirname))

    index = FileDB()
    n = 0
    errors = 0
    for dirname in args.dirnames:
        x, y = index.add_directory(dirname)
        n += x
        errors += y
    print(n, "files indexed")
    if errors > 0:
        print(errors, "files could not be indexed")


def cmd_search(args):
    verbose = not args.quiet
    found = True
    index = FileDB()
    for path in args.filenames:
        absp = os.path.abspath(path)
        if os.path.isdir(path):
            if verbose:
                print(path, "is a directory")
            found = False
            continue
        if os.path.getsize(path) < 20:
            if verbose:
                print(path, "is too small (<20)")
            found = False
            continue
        hash = filehash(path)
        found_one = False
        for found_path in index.search_hash(hash):
            if os.path.exists(found_path) and absp != os.path.abspath(found_path):
                found_one = True
                if verbose:
                    link = "L==>" if os.path.samefile(path, found_path) else " ==>"
                    print(path, link, found_path)
        if not found_one:
            if verbose:
                print(path, "NOT FOUND")
            found = False
    index.close()
    if found:
        sys.exit(0)
    else:
        sys.exit(1)


def paths_iter(paths, recursive):
    for path in paths:
        if os.path.islink(path):
            continue
        if os.path.isfile(path):
            yield path
            continue
        if os.path.isdir(path) and not recursive:
            print(path, "is a directory (perhaps use -r/--recursive?)")
            continue
        assert recursive
        for root, dirs, files in os.walk(path):
            for f in files:
                p = os.path.join(root, f)
                # skip fifos etc
                if not os.path.isfile(p) or os.path.islink(p):
                    continue
                yield p


def cmd_deletedups(args):
    index = FileDB()
    for path in paths_iter(args.filenames, args.recursive):
        absp = os.path.abspath(path)
        if os.path.getsize(path) < 20:
            print(path, "is too small (<20)")
            continue
        hash = filehash(path)
        found = False
        for found_path in index.search_hash(hash):
            if os.path.exists(found_path) and absp != os.path.abspath(found_path):
                found = True
                link = "L==>" if os.path.samefile(path, found_path) else " ==>"
                print(path, link, found_path)
        if found:
            trash(path, args.pretend)
        else:
            print(path, "not found")
    index.close()


def cmd_show(args):
    index = FileDB()
    n = 0
    for path, hash in index.all():
        print(hash, path)
        n += 1
    print(n, "entries")
    index.close()


def cmd_unknown(args):
    """Find unknown files"""
    index = FileDB()
    checked = 0
    unknown = 0
    for root, dirs, files in os.walk(args.path):
        for f in files:
            path = os.path.join(root, f)
            # print('checking', path)
            if not os.path.isfile(path):
                continue
            checked += 1
            hash = filehash(path)
            found = index.search_hash(hash)
            if len(list(found)) == 0:
                unknown += 1
                print(path)
    print(checked, "files checked")
    print(unknown, "files are unknown")
    print()
    print(index.count(), "total files in the index")


def cmd_stats(args):
    """Print statistics"""
    index = FileDB()
    print(index.count(), "files in the index")
    print("Indexed directories:")
    for d in index.dirs():
        print(d)


def cmd_dups(args):
    """Find duplicates"""
    index = FileDB()
    cursor = index.connection.cursor()
    # select * from files group by hash having count(distinct inode) > 1 order by size;


def main():
    parser = ArgumentParser(description=__doc__)
    subparsers = parser.add_subparsers(dest="subcommand")
    subparsers.required = True

    parser_index = subparsers.add_parser("index", help="indexing help")
    parser_index.add_argument("dirnames", nargs="+", help="name of directory to index")
    parser_index.set_defaults(func=cmd_index)

    parser_search = subparsers.add_parser("search", help="search help")
    parser_search.add_argument("--quiet", "-q", action="store_true")
    parser_search.add_argument("filenames", nargs="+")
    parser_search.set_defaults(func=cmd_search)

    parser_delete = subparsers.add_parser("deletedups", help="")
    parser_delete.add_argument(
        "--notest",
        dest="pretend",
        action="store_false",
        default=True,
        help="Actually delete files (don't pretend).",
    )
    parser_delete.add_argument(
        "--recursive",
        "-r",
        action="store_true",
        default=False,
        help="Recurse into subdirectories",
    )
    parser_delete.add_argument("filenames", nargs="+")
    parser_delete.set_defaults(func=cmd_deletedups)

    parser_unknown = subparsers.add_parser(
        "unknown", help="find unknown files in a directory"
    )
    parser_unknown.add_argument("path")
    parser_unknown.set_defaults(func=cmd_unknown)

    parser_show = subparsers.add_parser("show", help="")
    parser_show.set_defaults(func=cmd_show)

    parser_stats = subparsers.add_parser("stats", help="")
    parser_stats.set_defaults(func=cmd_stats)
    args = parser.parse_args()
    args.func(args)


if __name__ == "__main__":
    main()
