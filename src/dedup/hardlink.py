#!/usr/bin/env python3
"""
Find duplicate files and save space by hardlinking them

Files are considered duplicate if their MD5 checksums match.

If the two hardlinked files have different modification times,
the links are created in such a way that the modification time
of the older file is kept.

Anything else (creation and times, permissions, owners) is ignored
when comparing the files and (unless modification times are different),
which file is hardlinked to which is determined by traversal order.
"""
__author__ = "Marcel Martin"
__version__ = "0.1"

from argparse import ArgumentParser
import sys
import os
from fnmatch import fnmatch
import hashlib
from collections import defaultdict
from itertools import chain
import time
import random
import logging
from .utils import FileSet, filehash


logger = logging.getLogger(__name__)


def add_arguments(parser):
    arg = parser.add_argument
    parser.add_argument(
        "--notest",
        dest="pretend",
        action="store_false",
        default=True,
        help="Actually hard link files (don't pretend).",
    )
    parser.add_argument(
        "--ignore",
        dest="ignore_pattern",
        default=None,
        help="A pattern that describes files to ignore. For ex., '*.svn-base'",
    )
    parser.add_argument(
        "--minsize",
        type=int,
        default=1000,
        help="Minimum file size. Smaller files are ignored (default: %(default)s).",
    )
    parser.add_argument(
        "dirnames", nargs="+", help="names of directories in which to find duplicates"
    )


class Error(Exception):
    pass


def print_path(path1, path2):
    prefix = os.path.commonprefix([path1, path2])
    if prefix:
        print(f"{prefix}{{{path1[len(prefix):]} => {path2[len(prefix):]}}}")
    else:
        print(f"{path1} => {path2}")


def hardlink(path1, path2, pretend=True):
    """
    Make path1 and path2 hardlinks to the same file.
    """
    assert not os.path.samefile(path1, path2)
    info1 = os.stat(path1)
    info2 = os.stat(path2)
    if info1.st_dev != info2.st_dev:
        raise Error(f"Files '{path1}' and '{path2}' are not on the same partition")
    if info1.st_mtime > info2.st_mtime:
        path1, path2 = path2, path1
        info1, info2 = info2, info1
    # from here, path1 is the one to keep
    assert os.stat(path1).st_mtime <= os.stat(path2).st_mtime

    # create a temporary hard link
    tmppath = os.path.join(
        os.path.dirname(path1), ".finddups-temp-%05d" % (random.randint(1, 99999))
    )
    if os.path.exists(tmppath):
        raise IOError("path exists!")
    try:
        print_path(path1, path2)
    except UnicodeEncodeError:
        print("encoding problem")  # TODO!
    if not pretend:
        try:
            os.link(path1, tmppath)
        except OSError as e:
            print(f"cannot link {path1} to {tmppath}: {e}")
            sys.exit(2)
        assert filehash(tmppath) == filehash(path2)
        assert os.stat(path2).st_mtime >= os.stat(tmppath).st_mtime
        os.rename(tmppath, path2)

    # print(time.ctime(info1.st_mtime), "!=", time.ctime(info2.st_mtime))
    # directory, name = os.path.split(path1)


def main(args):
    if args.pretend:
        print("Option --notest not given: Not changing the file system (pretend mode)")

    # ignore_regex = re.compile(args.ignore_pattern)
    ignore_pattern = args.ignore_pattern
    for path in args.dirnames:
        if not os.path.isdir(path):
            parser.error("{} is not a directory".format(path))
    fileset = FileSet()
    saved = 0
    prevsaved = 0
    for root, dirs, files in chain(*(os.walk(path) for path in args.dirnames)):
        root_printed = False
        for f in files:
            if ignore_pattern and fnmatch(f, ignore_pattern):
                continue
            path = os.path.join(root, f)
            if os.path.islink(path):
                # skip symlinks
                continue
            d = fileset.duplicates_of(path)
            d = [p for p in d if p != path]
            if len(d) > 0:
                if not root_printed:
                    try:
                        print(root)
                    except UnicodeEncodeError:
                        pass
                    root_printed = True
                for x in d:
                    # skip symbolic links
                    same = os.path.samefile(path, x)
                    if not same:
                        # print("  %s == %s" % (path, x))
                        hardlink(path, x, pretend=args.pretend)
                        saved += os.path.getsize(path)
                        if saved > prevsaved + 1024 * 1024 * 10:
                            prevsaved = saved
                            print(f"saved disk space: {saved / 1024 / 1024:.3f} MiB")
            if os.stat(path).st_size >= args.minsize:
                fileset.add(path)
    if args.pretend:
        print()
        print("Nothing done. Use --notest to actually hard link files")
        message = "would have been saved"
    else:
        message = "saved"
    print(f"{saved / 1024 / 1024:.3f} MiB disk space {message}")
