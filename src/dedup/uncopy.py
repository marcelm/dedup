#!/usr/bin/env python3
"""
Delete duplicate files from target

Files in the target directory are deleted that also exist in the source
directory.
"""
import os
import logging

from .utils import create_index, walk_files, format_path

logger = logging.getLogger(__name__)


def directory_argument(path):
    if not os.path.isdir(path):
        raise ValueError("{} is not a directory".format(path))
    return path


def add_arguments(parser):
    arg = parser.add_argument
    arg(
        "--delete",
        dest="pretend",
        action="store_false",
        default=True,
        help="Actually delete files. If this is not given, only the names of "
        "files to delete are printed.",
    )
    arg(
        "--ignore",
        dest="ignore_pattern",
        default=None,
        help="A pattern that describes files to ignore. For ex., '*.svn-base'",
    )
    arg(
        "--minsize",
        type=int,
        default=256,
        help="Minimum file size. Smaller files are ignored (default: %(default)s).",
    )
    arg(
        "source",
        type=directory_argument,
        help="Source directory (will be left unchanged)",
    )
    arg("target", type=directory_argument, help="Target directory with files TO DELETE")


def main(args):
    # Build a file set for files in first subtree
    logger.info("Indexing source directory %r ...", args.source)
    fileset1 = create_index(args.source, args.minsize, args.ignore_pattern)
    logger.info(
        "Source contains %s files in %.2f MB, %s internal duplicates",
        len(fileset1),
        fileset1.size() / 1e6,
        fileset1.duplicates(),
    )

    logger.info("Inspecting target directory ...")
    removed = 0
    for path in walk_files(args.target, ignore_pattern=args.ignore_pattern):
        duplicates = fileset1.duplicates_of(path)
        if duplicates:
            if args.pretend:
                print("{!r} is copy of {}".format(path, duplicates))
            else:
                print("Removing {!r}".format(path))
                os.remove(path)
            removed += 1
    if args.pretend:
        logger.info("Nothing done. Use --delete to actually delete %s files", removed)
    else:
        logger.info("Deleted %s files", removed)
