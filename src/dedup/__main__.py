import sys
import logging
import importlib
from argparse import ArgumentParser, RawDescriptionHelpFormatter

COMMANDS = [
    "hardlink",
    "compare",
    "uncopy",
    "youngest",
    "date",
]

logger = logging.getLogger(__name__)


class HelpfulArgumentParser(ArgumentParser):
    """An ArgumentParser that prints full help on errors."""

    def __init__(self, *args, **kwargs):
        if "formatter_class" not in kwargs:
            kwargs["formatter_class"] = RawDescriptionHelpFormatter
        super().__init__(*args, **kwargs)

    def error(self, message):
        self.print_help(sys.stderr)
        args = {"prog": self.prog, "message": message}
        self.exit(2, "%(prog)s: error: %(message)s\n" % args)


def main(arguments=None):
    logging.basicConfig(level=logging.INFO, format="%(levelname)s: %(message)s")
    parser = HelpfulArgumentParser(description=__doc__, prog="dedup")
    parser.add_argument("--version", action="version", version="%(prog)s " + "0.1")

    subparsers = parser.add_subparsers()
    for command_name in COMMANDS:
        module = importlib.import_module("." + command_name, "dedup")
        subparser = subparsers.add_parser(
            command_name, help=module.__doc__.split("\n")[1], description=module.__doc__
        )
        subparser.set_defaults(func=module.main)
        module.add_arguments(subparser)

    args = parser.parse_args(arguments)
    if not hasattr(args, "func"):
        parser.error("Please provide the name of a subcommand to run")
    else:
        args.func(args)


if __name__ == "__main__":
    main()
